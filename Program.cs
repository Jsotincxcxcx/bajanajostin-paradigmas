﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ParadigmasDeProgramacion
{
    class Program
    {

        public class Ajedrecistas
        {
            public string NombresApellido { get; set; }
            public string Edad { get; set; }
            public string Pais { get; set; }
            public bool GranMaestro { get; set; }
        }
        static void Main(string[] args)
        {
            var ListadeAjedrecistas = new List<Ajedrecistas>()
            {
                new Ajedrecistas(){NombresApellido="Magnus Carlsen",
                Edad="31 Años",
                Pais = "Noruega",
                GranMaestro=true
                },
                new Ajedrecistas(){NombresApellido="Garry kasparov",
                Edad="58 Años",
                Pais = "Rusia",
                GranMaestro=true
                },
                new Ajedrecistas(){NombresApellido="Hikaru Nakamura",
                Edad="34 Años",
                Pais = "Japon",
                GranMaestro=true
                },
                new Ajedrecistas(){NombresApellido="Manuel Reyes",
                Edad="20 Años",
                Pais = "Noruega",
                GranMaestro=false },
              
            };
           
            // Paradigma Imperativo) //Como lo hace 
            var NombresdeGranMaestros = new List<string>();
            foreach (var Ajedrecistas in ListadeAjedrecistas)
            {
                if (Ajedrecistas.GranMaestro)
                    NombresdeGranMaestros.Add(Ajedrecistas.NombresApellido);
                
            }
            // Paradigma Funcional // Pedir lo que quiere
            var NombresdeGranMaestrosxd = ListadeAjedrecistas.Where(b => b.GranMaestro)
                .Select(b => b.NombresApellido);
           

            Console.ReadKey();

        }

    }
   
}


